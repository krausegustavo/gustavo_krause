
export function $selector(selector, scope) {
	return (scope || document).querySelector(selector);
}

export function $selectorAll(selector, scope) {
	return (scope || document).querySelectorAll(selector);
}

export function $on(target, type, callback) {
  target.addEventListener(type, callback);
}

export function $location(url) {
	window.location = url;
}

export function $delegate(scope, selector, type, callback) {

  var itens = $selectorAll(selector, scope);

  for(let i = 0; i <= itens.length - 1; i++) {
    $on(itens[i], type, callback);
  }
}

export function isHidden(el) {
    var style = window.getComputedStyle(el);
    return (style.display === 'none')
}
