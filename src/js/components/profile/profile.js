import template from "./profile.html";
import { $selector, $on, $location, $delegate } from '../../helpers';

import { getUser, updateUser, logout } from '../../services';

// sub components
import { ProfileEditPopover } from './profile-edit-popover';


export default class Profile {
  constructor() {
    let user = getUser();
    this.template = eval('`' + template + '`');
  }

  create() {
    $delegate(document, '[data-action="logout"]', 'click', logout);
    $delegate(document, '[data-action="save"]', 'submit', this.updateItem);

    var myScroll = new IScroll('.menu__wrapper', { scrollX: true, scrollY: false });
  }

  updateItem(event) {
    event.preventDefault();
    let form = this;
    let user = getUser();

    // updates all value
    if(this.name === 'profile-edit-modal') {
      let fullname = $selector('[name="fullname"]', form);
      let website = $selector('[name="website"]', form);
      let phone = $selector('[name="phone"]', form);
      let location = $selector('[name="location"]', form);
      user.fullname = fullname.value;
      user.website = website.value;
      user.phone = phone.value;
      user.location = location.value;
    }

    // updates just one value
    if(this.name === 'profile-edit-item') {
      let field = $selector('.textfield__input', this);
      user[field.name] = field.value;
    }

    updateUser(user);
  }

  getTemplate() {
    return this.template;
  };
}


