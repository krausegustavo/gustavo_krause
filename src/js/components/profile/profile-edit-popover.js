import { $selector, $on, $location, $delegate } from '../../helpers';

class ComponentProfileEditPopover extends HTMLElement {

  constructor() {
    super();
  }

  createdCallback() {

    let id = this.dataset.id;
    let value = this.dataset.value;
    let label = this.dataset.label;
    let name = this.dataset.name;

    this.innerHTML = `
      <div class="profile-edit-item">
        <button data-popover="${id}" type="button" class="button button--edit show-large"><span class="ion-edit" aria-hidden="true"></span></button>
        <aside id="${id}" class="profile-info__edit-item popover hide">
          <form name="profile-edit-item" novalidate data-action="save">
            <div class="textfield">
              <input class="textfield__input" type="text" name="${name}" id="field_${id}" value="${value}">
              <label class="textfield__label" for="field_${id}">${label}</label>
            </div>
            <div class="popover__actions">
              <button data-popover="${id}" type="submit" class="button">Save</button>
              <button data-popover="${id}" type="button" class="button button--outlined">Cancel</button>
            </div>
          </form>
        </aside>
      </div>
    `;

  }

}

export var ProfileEditPopover = document.registerElement('profile-edit-popover', ComponentProfileEditPopover);
