import template from "./login.html"
import { $selector, $on, $location } from '../../helpers';

import { login } from '../../services';

export default class Login {
  constructor() {
    this.template = `${template}`;
  }

  create() {
    let form = $selector('.login');
    $on(form, 'submit', this.login);
  }

  login(event) {
    event.preventDefault();

    let email = $selector('#email').value;
    let password = $selector('#password').value;

    if(!login(email, password)) {
      console.log('login error')
    }
  }

  getTemplate() {
    return this.template;
  };
}


