import Route from './route';
import { $on } from './helpers';

const route = new Route();

const setRoute = () => route.setRoute(document.location.hash);

$on(window, 'load', setRoute);
$on(window, 'hashchange', setRoute);
