import Store from './store';

import { $location } from './helpers';

let store = new Store();

export function login(email, password) {
  if (email == 'login@example.com' && password == '123') {
    let state = store.get();
    state.user.isLoggedIn = true;
    store.set(state);
    $location('#/profile');
    return true;
  } else {
    return false;
  }
}

export function logout() {
  let state = store.get();
  state.user.isLoggedIn = false;
  store.set(state);
  $location('#/login');
}

export function isLoggedIn() {
  let state = store.get();
  return state.user.isLoggedIn;
}

export function getUser() {
  let state = store.get();
  return state.user;
}

export function updateUser(user) {
  let state = store.get();
  state.user = user;
  store.set(state);
}
