import { $selector, $location } from './helpers';

import { isLoggedIn, logout, getUser } from './services';

import { updateElements } from './elements';

import Login from "./components/login/login"
import Profile from "./components/profile/profile"


let routeActived = '';


export default class Route {

  constructor() {

    this.routes = [{
      path: 'login',
      component: Login
    }, {
      path: 'profile',
      component: Profile,
      private: true
    }, {
      path: '',
      component: Login
    }];

  }

  setRoute(hash) {
    const route = hash.replace(/^#\//, '');

    const selectRoute = this.routes.filter(view => view.path == route);
    routeActived = selectRoute[0];

    if (routeActived.private && !isLoggedIn()) {
      logout();
    } else {
      updateView();
    }
  }

}

export function updateView() {
  let view = new routeActived.component();
  $selector('#app').innerHTML = view.getTemplate();
  view.create();
  updateElements();
}
