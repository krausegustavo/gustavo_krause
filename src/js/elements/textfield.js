import { $selectorAll, $selector, $on, $delegate } from '../helpers';

export function bindTextfields() {

  function toggleFilled(event, element) {
    let field = element || event.target;
    if (field.value != '') {
      field.classList.add('textfield__input--filled');
    } else {
      field.classList.remove('textfield__input--filled');
    }
  }

  var itens = $selectorAll('.textfield__input');

  for(let i = 0; i <= itens.length - 1; i++) {
    toggleFilled(false, itens[i]);
    $on(itens[i], 'change', toggleFilled);
  }

}
