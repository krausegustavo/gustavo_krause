import { bindTextfields } from './textfield';
import { bindPopover } from './popover';
import { bindModal } from './modal';

export function updateElements() {
  bindTextfields();
  bindPopover();
  bindModal();
}
