import { $selectorAll, $selector, $delegate, $on, isHidden } from '../helpers';

export function bindPopover() {

  function togglePopover(event) {

    let button = this;
    let popoverId = button.dataset.popover;
    let popover = $selector(`#${popoverId}`);

    if(isHidden(popover)) {
      popover.classList.remove('hide');
    } else {
      popover.classList.add('hide');;
    }
  }

  $delegate(document, '[data-popover]', 'click', togglePopover);

}
