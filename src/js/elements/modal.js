import { $selectorAll, $selector, $delegate, $on, isHidden } from '../helpers';

export function bindModal() {

  function openModal(event) {
    let button = this;
    let id = button.dataset.modal;
    let modal = $selector(`#${id}`);

    if(isHidden(modal)) {
      modal.classList.remove('hide');
    }
  }

  function closeModal(event) {
    let button = this;
    let id = button.dataset.closeModal;
    let modal = $selector(`#${id}`);
    if(!isHidden(modal)) {
      modal.classList.add('hide');
    }
  }

  $delegate(document, '[data-modal]', 'click', openModal);
  $delegate(document, '[data-close-modal]', 'click', closeModal);

}
