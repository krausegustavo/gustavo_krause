import { updateView } from './route';

export default class Store {
  constructor() {
    this.store = {
      user: {
        fullname: 'Jessica Parker',
        phone: '(949) 325 - 68594',
        location: 'Newport Beach, CA',
        category: 'Business',
        website: 'www.seller.com',
        isLoggedIn: false,
        reviews: 6,
        followers: 15
      }
    };
  }

  get() {
    return this.store;
  }

  set(store) {
    this.store = store;
    updateView();
  }

}


