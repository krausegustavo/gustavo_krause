# Gustavo_Krause project

##### Setup

on terminal: `npm install`

##### Develop

- Run `gulp` to watch the scss files;
- Execute `npm run watch` in another terminal - to watch the js files.
- Execute `npm start` in another terminal - to start the dev server;
- Open: `http://localhost:8080`

##### Login

- email: `login@example.com`
- password: `123`
- demo: https://gustavokrause.github.io/Gustavo_Krause.github.io/

##### Build

Run `npm run build` to build the production version.
