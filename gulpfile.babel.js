'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean';
import sourcemaps from 'gulp-sourcemaps';

const dirs = {
  src: 'src',
  dest: 'dist'
};

const sassPaths = {
  src: `${dirs.src}/styles/**/*.scss`,
  dest: `${dirs.dest}/styles/`
};

gulp.task('styles', () => {
  return gulp.src(sassPaths.src)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(sassPaths.dest));
});

gulp.task('styles:clean', () => {
    return gulp.src(sassPaths.dest, {read: false})
      .pipe(clean());
});

gulp.task('styles:watch', () => {
    gulp.watch(sassPaths.src, ['styles']);
});

gulp.task('styles:build', ['styles:clean', 'styles']);

gulp.task('default', ['styles:build', 'styles:watch']);
